// db.js
const mysql = require("mysql2");
require("dotenv").config();

// Create a connection pool

const pool = mysql.createPool({
  host: "localhost",
  user: "root",
  password: process.env.MYSQL_PASSWORD,
  database: "auth",
});

// Export the connection pool to be used by other modules
module.exports = pool.promise();
