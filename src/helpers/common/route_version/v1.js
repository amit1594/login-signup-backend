const { Router } = require("express");
const { appAuthRouterV1 } = require("../../../routes/auth/auth_routes");

const v1 = Router();
v1.use("/authroutes", appAuthRouterV1);

module.exports = {
  v1,
};
