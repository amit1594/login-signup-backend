// // Import Packages
const joi = require("joi");

const appAuthSignupSchema = joi.object({
  email: joi.string().trim().required(),
  password: joi.string().trim().required(),
  confirm_password: joi.string().trim().required(),
});

const appAuthLoginSchema = joi.object({
  email: joi.string().trim().required(),
  password: joi.string().trim().required(),
});

// Export schema
module.exports = {
  appAuthSignupSchema,
  appAuthLoginSchema,
};
