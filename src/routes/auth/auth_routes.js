const { Router } = require("express");
const {
  loginUser,
  signupUser,
} = require("../../controllers/auth/auth_controller");
const appAuthRouterV1 = Router();

appAuthRouterV1.post("/login-user", loginUser);

appAuthRouterV1.post("/signup-user", signupUser);

module.exports = {
  appAuthRouterV1,
};
