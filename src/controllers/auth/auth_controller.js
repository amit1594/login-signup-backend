const {
  appAuthLoginSchema,
  appAuthSignupSchema,
} = require("../../helpers/joi/auth/auth_joi");
const httpErrors = require("http-errors");
var database = require("../../helpers/common/init_mysql");
var jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");
const JWT_SECTRT = "amityadav";
require("dotenv").config();

const signupUser = async (req, res) => {
  try {
    //validate joi schema
    const appAuthDetails = await appAuthSignupSchema.validateAsync(req.body);

    //create new user
    const [rows_table] = await database.execute(`SHOW TABLES LIKE 'user'`);

    if (appAuthDetails.password !== appAuthDetails.confirm_password) {
      throw httpErrors.Conflict(`Password is not matched`);
    }
    const salt = await bcrypt.genSalt(10);
    const secPass = await bcrypt.hash(appAuthDetails.password, salt);

    if (rows_table.length <= 0) {
      await database.query(`CREATE TABLE user (
              id INT PRIMARY KEY AUTO_INCREMENT,
              email VARCHAR(50) NOT NULL,
              password VARCHAR(150) NOT NULL,
              UNIQUE (email)
              )`);
    }
    let user = await database.query(
      `SELECT * FROM user WHERE email='${appAuthDetails.email}'`
    );
    if (user[0].length > 0) {
      throw httpErrors.Conflict(`User with this email is already exist.`);
    }
    const [rows1] = await database.query(
      `INSERT INTO user (email, password) VALUES ('${appAuthDetails.email}', '${secPass}')`
    );

    // Send Response
    res.status(200).send({
      error: false,
      data: {
        data: rows1,
        message: "User created successfully.",
      },
    });
  } catch (error) {
    res.status(200).send({
      error: true,
      reason: error,
    });
  }
};

// Login User
const loginUser = async (req, res) => {
  try {
    const appAuthDetails = await appAuthLoginSchema.validateAsync(req.body);
    console.log(appAuthDetails, JWT_SECTRT, "JWT_SECTRT");
    let user = await database.query(
      `SELECT * FROM user WHERE email='${appAuthDetails.email}'`
    );

    if (user[0].length <= 0) {
      throw httpErrors.Conflict(`User with this email is not exist.`);
    }

    const passwordCompare = await bcrypt.compare(
      appAuthDetails.password,
      user[0][0].password
    );
    if (!passwordCompare) {
      throw httpErrors.Conflict(`Please try to login with correct credential.`);
    }
    const data = {
      user: {
        id: appAuthDetails.email,
      },
    };
    const authtoken = jwt.sign(data, JWT_SECTRT);

    res.status(200).send({
      error: false,
      data: {
        token: authtoken,
        message: "User Login successfully.",
      },
    });
  } catch (error) {
    res.status(200).send({
      error: true,
      reason: error,
    });
  }
};

module.exports = {
  loginUser,
  signupUser,
};
